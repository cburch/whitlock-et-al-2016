# README #

This repository contains the Python code used to run the simulations in:

Whitlock, A. O. B., K. M. Peck, R. B. R. Azevedo, and C. L Burch. 2016. An Evolving Genetic Architecture Interacts with Hill-Robertson Interference to Determine the Benefit of Sex. Genetics. DOI: http://dx.doi.org/10.1534/genetics.116.186916
## Files Included: ##
* genotype.py | the Genotype class 
* population.py | the Population class
* config.py | parameter specification
* generate_founders.py | generate collections of 50 random founders
* Population_3.pickle | pickled collection of 50 random founders
* evolution.py | simulate Evolution of Sexual or Asexual Populations
* Metapopulation_Founder_0_10000_23325_0.pickle | pickled equilibrium asexual population
* invasion.py | simulate Invasions of a modifier of sex into an equilibrium population
* rec_modifier_evolution.py | simulate Evolution of a weak modifier of recombination ###
* rec_modifier_invasion.py | simulate Invasions of a weak modifier of recombination into an equilibrium asexual population

## Running the code ##

### Generate founders: ###

1. Place genotype.py, population.py, config.py, and generate_founders.py together in the same folder.
2. Generate multiple sets of 50 random founders using the command:
 
> python generate_founders.py

### Simulate Evolution: ###

1. We used as our starting point a single founder set (Population_3.pickle) that showed mean and variance in mean mutation effects that were representative of a much larger (n=1,000) collection of random gene networks.
2. Place genotype.py, population.py, config.py, Population_3.pickle and evolution.py together in the same folder.
3. Run 10,000 generation evolution simulations using the command:

> python evolution.py

### Simulate Invasions: ###

1. Place genotype.py, population.py, config.py, invasion.py and Metapopulation_Founder_0_10000_23325_0.pickle together in the same folder.
2. Run 100 replicate invasion trials using the command:

> python invasion.py

## Code authors: ##
* Alexander Whitlock, a.b.whitlock@gmail.com
* Kayla Peck, kaylap@live.unc.edu
* Ricardo Azevedo, razevedo@uh.edu
* Christina Burch, cburch@bio.unc.edu