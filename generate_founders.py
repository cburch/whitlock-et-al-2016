# -*- coding: utf-8 -*-
import population
import genotype
import numpy as np
import os
import errno
import config

number_of_founder_sets = 50
number_of_founders = 50
config.pop_size = number_of_founders

directory_name = "founders_0.08"

output_header = []
output_header.append("individual")
output_header.append("fitness")
output_header.append("frequency_lethal") 
output_header.append("frequency_neutral")
output_header.append("mean_mutant_fitness")

try:
    os.makedirs(directory_name)
except OSError as exception:
    if exception.errno != errno.EEXIST:
	raise
os.chdir(directory_name)

for i in range(1,number_of_founder_sets+1):
    founder = population.Matrix_population.generate_founder()
    founding_pop = population.Matrix_population.found_clonal_matrix_population(founder)
    founding_pop.organisms[0].calculate_mutant_fitness_distribution(founding_pop.organisms[0].mean_expression_pattern)
    
    output_file = open("data_gen" + str(i) + ".txt", "w")
    output_file.write('\t'.join(map(str,output_header))+'\n')
 
    output_list = []
    output_list.append(0)
    for dat in range(1, len(output_header)):
        output_list.append(getattr(founding_pop.organisms[0], output_header[dat]))
    output_file.write('\t'.join(map(str,output_list))+'\n')

    for j in range(1, number_of_founders):
        founding_pop.organisms[j] = population.Matrix_population.generate_founder()
        founding_pop.organisms[j].calculate_fitness(founding_pop.organisms[j].mean_expression_pattern)
        founding_pop.organisms[j].calculate_mutant_fitness_distribution(founding_pop.organisms[j].mean_expression_pattern)
        output_list = []
        output_list.append(j)
        for dat in range(1, len(output_header)):
                output_list.append(getattr(founding_pop.organisms[j], output_header[dat]))
        output_file.write('\t'.join(map(str,output_list))+'\n')
        
    output_file.close() 
    founding_pop._pickle(i)
