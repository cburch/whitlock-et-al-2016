cost_of_sex = 1
mutation_rate = 1.0
neutral_locus_mutation_rate = 1
num_loci = 1 # number of neutral loci
selection_strength = 0.2
n_genes = 100
connectivity = 0.05
num_generations = 10000
recording_generations = [0,1,2,3,6,10,18,32,56,100,178,316,562,1000,1778,3162,5623,10000]

meta_pop_size = 10 
n_demes = 1
founder_sex_locus = 0
foundernum = 0
migration_rate = 0
pop_size = meta_pop_size / n_demes
n_migrants = migration_rate * pop_size

# parameters for measuring mutation effects
replicates = 100 # number of mutations sampled for each individual in the measure of mutation effects
number_of_mutations = 1 # set to 1 if not measuring epistasis between multiple mutations

# parameters for modifiers of RECOMBINATION
free_recombination = 1 # set to 1 for modifier of sex, 0 for modifier of recombination
crossover_rate = 0 # use only for modifiers of recombination
crossover_rate_mutation_rate = 0 # use only if crossover rate is allowed to evolve

# parameters for measuring within-deme genetic variation and total metapopulation genetic variation
n_deme_draws = 10
n_meta_draws = 10