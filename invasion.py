# -*- coding: utf-8 -*-
import population
import os
import genotype
import config
import fnmatch
import copy
import random
import numpy as np

#set parameters from config file
pop_size = config.pop_size
n_demes = config.n_demes
n_migrants = config.n_migrants
foundernum = config.foundernum
cost = config.cost_of_sex 

#open file for data storage and write header
pid = os.getpid()
population_datafile = open("separate_invasion_data_cost_"+str(cost)+"_founder_"+str(foundernum)+"_"+str(pid)+".txt","a")
population_datafile.write("Founder"+"\t"+"Rep"+"\t"+"Gen"+"\t"+"Sex Frequency"+"\t"+"Mean_Sexual_Fitness"+"\t"+"Mean_Asexual_Fitness"+"\n")
population_datafile.close()

#open pickled pop
for file in os.listdir('.'): #match to specific files
    if fnmatch.fnmatch(file, 'Metapopulation*'):
        metapop1 = population.Metapopulation._unpickle_metapopulation(str(file))
        print(str(file))
optimum = metapop1.deme_list[0].optimal_exp_state
for deme in metapop1.deme_list:
    for org in deme.organisms:
        org.assign_properties()
print(optimum)

#invade it
#loop over replicates
for rep in range(100):
    metapop = copy.deepcopy(metapop1)
    deme_number = random.randint(0,n_demes-1)
    deme_size = len(metapop.deme_list[deme_number].organisms)
    mut = random.randint(0,deme_size-1)
    print "deme "+str(deme_number)+",deme size "+str(deme_size)+",mutant "+str(mut)
    org = metapop.deme_list[deme_number].organisms[mut]
    org.sex_locus = (org.sex_locus+1)%2
    population_datafile = open("separate_invasion_data_cost_"+str(cost)+"_founder_"+str(foundernum)+"_"+str(pid)+ ".txt","a")
    #begin evolution
    current_gen_num = 0
    sexual_fitnesses = []
    asexual_fitnesses = []    
    for i in xrange(config.n_demes):
        metapop.deme_list[i].get_population_fitness()
        sexual_fitnesses.extend([org.fitness for org in metapop.deme_list[i].organisms if org.sex_locus == 1.0])
        asexual_fitnesses.extend([org.fitness for org in metapop.deme_list[i].organisms if org.sex_locus == 0.0])
    mean_sexual_fitness = np.mean(sexual_fitnesses)
    mean_asexual_fitness = np.mean(asexual_fitnesses)
    while not metapop.measure_sex_fixation(pid, current_gen_num):
        population_datafile.write(str(foundernum) + "\t" + str(rep) + "\t" + str(current_gen_num) + "\t" + str(metapop.prop_sex) + "\t" + str(mean_sexual_fitness) + "\t" + str(mean_asexual_fitness) + "\n")
        metapop.migrate(n_migrants)
        sexual_fitnesses = []
        asexual_fitnesses = []
        for i in xrange(config.n_demes):
            #uncomment only one of the next 3 lines to specify Reproductive Mode
            metapop.deme_list[i].reproduce_pop_by_recombination_probability() #separate sex
            #metapop.deme_list[i].reproduce_pop_by_dominant_recombination_probability() #dominant sex
            #metapop.deme_list[i].reproduce_pop_by_recessive_recombination_probability() #recessive sex
                        
            metapop.deme_list[i].get_population_fitness()
            sexual_fitnesses.extend([org.fitness for org in metapop.deme_list[i].organisms if org.sex_locus == 1.0])
            asexual_fitnesses.extend([org.fitness for org in metapop.deme_list[i].organisms if org.sex_locus == 0.0])
        mean_sexual_fitness = np.mean(sexual_fitnesses)
        mean_asexual_fitness = np.mean(asexual_fitnesses)
        current_gen_num += 1

    population_datafile.write(str(foundernum) + "\t" + str(rep) + "\t" + str(current_gen_num) + "\t" + str(metapop.prop_sex) + "\t" + str(mean_sexual_fitness) + "\t" + str(mean_asexual_fitness) + "\n")
    population_datafile.close()
    print metapop.prop_sex
