# -*- coding: utf-8 -*-
import population
import os
import genotype
import config
import fnmatch
import copy
import random
import numpy as np

#set parameters from config file
pop_size = config.pop_size
n_demes = config.n_demes
n_migrants = config.n_migrants
foundernum = config.foundernum
cost = config.cost_of_sex 
crossover_rate = config.crossover_rate
crossover_rate_mutation_rate = config.crossover_rate_mutation_rate
free_recombination = config.free_recombination

#open file for data storage and write header
pid = os.getpid()
population_datafile = open("rec_modifier_invasion_data_cost_"+str(cost)+"_founder_"+str(foundernum)+"_"+str(pid)+".txt","a")
population_datafile.write("Founder"+"\t"+"Rep"+"\t"+"Gen"+"\t"+"Fixed Crossover Rate"+"\n")
population_datafile.close()

#open pickled pop
for file in os.listdir('.'): #match to specific files
    if fnmatch.fnmatch(file, 'Metapopulation*'):
        filename = str(file)
        generation = filename.split('_')[3]
        if generation == "10000":        
            print(filename)
            metapop1 = population.Metapopulation._unpickle_metapopulation(filename)
            optimum = metapop1.deme_list[0].optimal_exp_state
            print(optimum)

            for deme in metapop1.deme_list:
                deme.get_population_fitness()

                for org in deme.organisms:
                    org.assign_properties()
                    org.assign_chromosome_properties(org.n_genes, len(org.neutral_locus)) 

            #invade it
            #loop over replicates
            for rep in range(1000):
                metapop = copy.deepcopy(metapop1)
                deme_number = random.randint(0,n_demes-1)
                deme_size = len(metapop.deme_list[deme_number].organisms)
                mut = random.randint(0,deme_size-1)
                print "mutant "+str(mut)
                org = metapop.deme_list[deme_number].organisms[mut]
                org.crossover_rate = org.crossover_rate + 0.1
                #begin evolution
                current_gen_num = 0
                test = 0.05
                while ((str(test)!="0.1") and (test > 0)):
                    print test
		    metapop.migrate(n_migrants)
                    mean_crossover_rate = []
                    for i in xrange(config.n_demes):
                        metapop.deme_list[i].reproduce_pop_by_crossover_probability()
                        metapop.deme_list[i].get_population_fitness()
                        mean_crossover_rate.append(np.mean([org.crossover_rate for org in metapop.deme_list[i].organisms]))
                    current_gen_num += 1
                    test = np.mean(mean_crossover_rate)
                population_datafile = open("rec_modifier_invasion_data_cost_"+str(cost)+"_founder_"+str(foundernum)+"_"+str(pid)+".txt","a")
                population_datafile.write(str(foundernum) + "\t" + str(rep) + "\t" + str(current_gen_num) + "\t" + str(test) + "\n")
                population_datafile.close()
                print test
            
